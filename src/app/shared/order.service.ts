import { Injectable } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import {NotificationService} from './notification.service'

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  orderList: AngularFireList<any>;
  notification : NotificationService;
  constructor(private firebase: AngularFireDatabase) { }
  form: FormGroup = new FormGroup({
    $key: new FormControl(null),
    customerName: new FormControl(''),
    noOfItem: new FormControl(''),
    amount: new FormControl(''),
    totalAmount: new FormControl(''),
    Status: new FormControl(''),
    itemName: new FormControl(''),
    address: new FormControl('')
  });

  getOrderList() {
    this.orderList = this.firebase.list('pizza');
    return this.orderList.snapshotChanges();
  }

  insertOrder(order) {
    this.orderList.push({
      customerName: order.customerName,
      noOfItem: order.noOfItem,
      amount: order.amount,
      totalAmount: order.totalAmount,
      Status: order.Status,
      itemName: order.itemName,
      address: order.address
    })
  }

  updateOrder(order){
    this.orderList.update(order.$key,
      {
        customerName: order.customerName,
        noOfItem: order.noOfItem,
        amount: order.amount,
        totalAmount: order.totalAmount,
        Status: order.Status,
        itemName: order.itemName,
        address: order.address
      });
      return this.orderList.snapshotChanges();
  }
}
