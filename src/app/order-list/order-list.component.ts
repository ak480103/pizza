import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { TooltipPosition } from '@angular/material/tooltip';
import { NotificationService } from '../shared/notification.service';
import { OrderService } from '../shared/order.service';


@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.css']
})
export class OrderListComponent implements OnInit {
  orderList;
  displayedColumns: string[] = ['CustomerName', 'noOfItem', 'totalAmt', 'status', 'action'];
  status: string[] = ["Order Received", "Preparing", "Ready to serve"];
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  position = new FormControl(this.positionOptions[2]);
  Item_Name:string="";
  amt:string="";
total_amt:string="";
cust_name:string="";
cust_address:string="";
  constructor(
    private service: OrderService,
    public notification : NotificationService) { }

  ngOnInit(): void {
    this.service.getOrderList().subscribe(list => {
      let array = list.map(item => {
        return {
          $key: item.key,
          ...item.payload.val()
        }
      });
      this.orderList = array;
     

    });
  }

  updateStatus(order) {
    
    if (order.Status === "Ready to serve") {
      this.notification.error("Order is Ready to Serve");
    } else {
      let currentStat = this.status.indexOf(order.Status);
     
      order.Status = this.status[currentStat+1];
      this.service.updateOrder(order).subscribe((res : any)=>{
       this.notification.success("Status Updated");
      });
    }


  }
  orderDetail(order) {
    this.Item_Name = order.itemName;
    this.amt = order.amount;
    this.total_amt = order.totalAmount;
    this.cust_name = order.customerName;
    this.cust_address = order.address;
    
  }

  onClose(){
    this.Item_Name = "";
    this.amt = "";
    this.total_amt = "";
    this.cust_name = "";
    this.cust_address = "";
  }

}
